import 'package:flutter/material.dart';
import 'package:promovil/main.dart';
import 'package:promovil/pages/about.dart';
import 'package:promovil/pages/about_contact.dart';
import 'package:promovil/pages/consult.dart';
import 'package:promovil/pages/consult_customer.dart';
import 'package:promovil/pages/consult_document.dart';
import 'package:promovil/pages/consult_document_summary.dart';
import 'package:promovil/pages/consult_product.dart';
import 'package:promovil/pages/empty.dart';
import 'package:promovil/pages/exit.dart';
import 'package:promovil/pages/menu.dart';
import 'package:promovil/pages/order.dart';
import 'package:promovil/pages/setting.dart';
import 'package:promovil/pages/sync.dart';

import '../pages/order_consult.dart';
import '../pages/order_new.dart';

const String mainPage = '/';
const String menuPage = '/menu';
const String consultPage = '/consult';
const String consultCustomerPage = '/consult_customer';
const String consultProductPage = '/consult_product';
const String consultDocumentPage = '/consult_document';
const String consultDocumentSummaryPage = '/consult_document_summary';
const String orderPage = '/order';
const String orderNewPage = '/order/new';
const String orderConsultPage = '/order/consult';
const String syncPage = '/sync';
const String settingPage = '/setting';
const String aboutPage = '/about';
const String aboutContactPage = '/about_contact';
const String exitPage = '/exit';
const String emptyPage = '/empty';

Route<dynamic> controller(RouteSettings settings) {
  switch (settings.name) {
    case mainPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const MyApp());
    case menuPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Menu());

    case consultPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Consult());
    case consultCustomerPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const ConsultCustomer());
    case consultProductPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const ConsultProduct());
    case consultDocumentPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const ConsultDocument());
    case consultDocumentSummaryPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const ConsultDocumentSummary());

    case orderPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Order());
    case orderNewPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const OrderNew());
    case orderConsultPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const OrderConsult());

    case syncPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Sync());

    case settingPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Setting());

    case aboutPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const About());
    case aboutContactPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const AboutContact());

    case exitPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Exit());

    case emptyPage:
      return MaterialPageRoute(settings: settings, builder: (context) => const Empty());

    default:
      throw ('this route name does not exist');
  }
}