import 'package:flutter/material.dart';
import 'package:promovil/db/db_config.dart';
import 'package:promovil/pages/menu.dart';

import 'route/route.dart' as route;

Future<void> main() async {

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    ConfigDB().verifyConfig();

    return MaterialApp(
      title: 'ProMovil',
      theme: ThemeData(
        brightness: Brightness.light,
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.blueGrey
        ).copyWith(secondary: Colors.blueGrey
        ),
        primaryColor: Colors.blueGrey,
      ),
      onGenerateRoute: route.controller,
      initialRoute: route.mainPage,

      home: const Menu(),
    );
  }
}