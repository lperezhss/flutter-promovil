// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class DocumentSummaryDB extends Database {

  static const String finish = 'FINISH';

  Future<List<DocumentSummary>> allDocumentSummarys() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(documentSummarys)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertDocumentSummary(DocumentSummary documentSummary) {
    return into(documentSummarys).insert( documentSummary );
  }

  insertUpdateDocumentSummarys(List<DocumentSummary> lsDocumentSummary) async {
    // lsDocumentSummary.forEach( (DocumentSummary documentSummary) => into(documentSummarys).insertOnConflictUpdate( documentSummary ) );
    for (DocumentSummary documentSummary in lsDocumentSummary) {
      await into(documentSummarys).insertOnConflictUpdate( documentSummary );
    }
    return;
  }

  Future deleteAllDocumentSummarys() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(documentSummarys)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getDocumentSummarys() async {
    try {
      var url = Uri.parse(urlStr + 'document_summary');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<DocumentSummary> lsDocumentSummarys = jsonItems
            .map<DocumentSummary>((json) => DocumentSummary.fromJson(json))
            .toList();

        await deleteAllDocumentSummarys();
        await insertUpdateDocumentSummarys(lsDocumentSummarys);
      } else {
        throw Exception('Failed to load DocumentSummarys');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}