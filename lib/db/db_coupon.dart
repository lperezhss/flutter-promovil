// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:promovil/db/db_constant.dart';

import 'database.dart';

class CouponDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Coupon>> allCoupons() {
    return select(coupons).get();
  }

  Future insertCoupon(Coupon coupon) {
    return into(coupons).insert( coupon );
  }

  insertUpdateCoupons(List<Coupon> lsCoupon) async {
    // lsCoupon.forEach( (CouponsCompanion coupon) => into(coupons).insertOnConflictUpdate( coupon ) );
    for (Coupon coupon in lsCoupon) {
      await into(coupons).insertOnConflictUpdate( coupon );
    }
    return;
  }

  Future deleteAllCoupons() {
    // delete the oldest nine tasks
    return ( delete(coupons)..where( (tbl) => tbl.coupon.isNotNull() ) ).go();
  }


  Future<String> getCoupons() async {
    try {
      var url = Uri.parse(ConstantDB.getURL() + 'coupon');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Coupon> lsCoupons = jsonItems
            .map<Coupon>((json) => Coupon.fromJson(json))
            .toList();

        await deleteAllCoupons();
        await insertUpdateCoupons(lsCoupons);
      } else {
        throw Exception('Failed to load Coupons');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}