// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class ProductDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Product>> allProducts() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(products)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertProduct(Product product) {
    return into(products).insert( product );
  }

  insertUpdateProducts(List<Product> lsProduct) async {
    // lsProduct.forEach( (Product product) => into(products).insertOnConflictUpdate( product ) );
    for (Product product in lsProduct) {
      await into(products).insertOnConflictUpdate( product );
    }
    return;
  }

  Future deleteAllProducts() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(products)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getProducts() async {
    try {
      var url = Uri.parse(urlStr + 'product');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Product> lsProducts = jsonItems
            .map<Product>((json) => Product.fromJson(json))
            .toList();

        await deleteAllProducts();
        await insertUpdateProducts(lsProducts);
      } else {
        throw Exception('Failed to load Products');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}