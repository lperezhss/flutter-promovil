// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class DocumentDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Document>> allDocuments() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(documents)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future<List<Document>> getDocumentByCustomers(String customer_id) async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(documents)..where( (tbl) => tbl.customer_id.equals(customer_id) & tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future<Customer> getCustomer(String customer_id) async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(customers)..where( (tbl) => tbl.id.equals(customer_id) & tbl.coupon.equals(activeCoupon) ) ).getSingle();
  }

  /*
  Future<List<Document>> getOrderComplete(String customer_id) async {
    Customer customer = await getCustomer(customer_id);
    SimpleCustomer simpleCustomer = SimpleCustomer.fromJson(customer.toJson());

    List<Document> documents = await getDocumentByCustomers(customer_id);
    List<SimpleDocument> simpleDocuments = documents
        .map<SimpleDocument>(
            (document) => SimpleDocument.fromJson(document.toJson()))
        .toList();

    SimpleCustomerDocument simpleCustomerDocument = new SimpleCustomerDocument(simpleCustomer, simpleDocuments);

    return documents;
  }
  */

  Future insertDocument(Document document) {
    return into(documents).insert( document );
  }

  insertUpdateDocuments(List<Document> lsDocuments) async {
    // documents.forEach( (DocumentsCompanion document) => into(documents).insertOnConflictUpdate( document ) );
    for (Document document in lsDocuments) {
      await into(documents).insertOnConflictUpdate( document );
    }
    return;
  }

  Future deleteAllDocuments() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(documents)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getDocuments() async {
    try {
      var url = Uri.parse(urlStr + 'document');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Document> lsDocuments = jsonItems
            .map<Document>((json) => Document.fromJson(json))
            .toList();

        await deleteAllDocuments();
        await insertUpdateDocuments(lsDocuments);
      } else {
        throw Exception('Failed to load Documents');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}