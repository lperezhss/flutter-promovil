// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class PaymentTypeDB extends Database {

  static const String finish = 'FINISH';

  Future<List<PaymentType>> allPaymentTypes() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(paymentTypes)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertPaymentType(PaymentType paymentType) {
    return into(paymentTypes).insert( paymentType );
  }

  insertUpdatePaymentTypes(List<PaymentType> lsPaymentType) async {
    // lsPaymentType.forEach( (PaymentType paymentType) => into(paymentTypes).insertOnConflictUpdate( paymentType ) );
    for (PaymentType paymentType in lsPaymentType) {
      await into(paymentTypes).insertOnConflictUpdate( paymentType );
    }
    return;
  }

  Future deleteAllPaymentTypes() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(paymentTypes)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getPaymentTypes() async {
    try {
      var url = Uri.parse(urlStr + 'payment_type');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<PaymentType> lsPaymentTypes = jsonItems
            .map<PaymentType>((json) => PaymentType.fromJson(json))
            .toList();

        await deleteAllPaymentTypes();
        await insertUpdatePaymentTypes(lsPaymentTypes);
      } else {
        throw Exception('Failed to load PaymentTypes');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}