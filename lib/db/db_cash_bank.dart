import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class CashBankDB extends Database {

  static const String finish = 'FINISH';

  Future<List<CashBank>> allCashBanks() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(cashBanks)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertCashBank(CashBank cashBank) {
    return into(cashBanks).insert( cashBank );
  }

  insertUpdateCashBanks(List<CashBank> lsCashBank) async {
    // lsCashBank.forEach( (CashBank cashBank) => into(cashBanks).insertOnConflictUpdate( cashBank ) );
    for (CashBank cashBank in lsCashBank) {
      await into(cashBanks).insertOnConflictUpdate( cashBank );
    }
    return;
  }

  Future deleteAllCashBanks() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(cashBanks)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getCashBanks() async {
    try {
      var url = Uri.parse(urlStr + 'cash_bank');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<CashBank> lsCashBanks = jsonItems
            .map<CashBank>((json) => CashBank.fromJson(json))
            .toList();

        await deleteAllCashBanks();
        await insertUpdateCashBanks(lsCashBanks);
      } else {
        throw Exception('Failed to load CashBanks');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}