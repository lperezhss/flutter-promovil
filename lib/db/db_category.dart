import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class CategoryDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Category>> allCategorys() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(categorys)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertCategory(Category category) {
    return into(categorys).insert( category );
  }

  insertUpdateCategorys(List<Category> lsCategorys) async {
    // lsCategorys.forEach( (Category lsCategory) => into(categorys).insertOnConflictUpdate( lsCategory ) );
    for (Category category in lsCategorys) {
      await into(categorys).insertOnConflictUpdate( category );
    }
    return;
  }

  Future deleteAllCategorys() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(categorys)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getCategorys() async {
    try {
      var url = Uri.parse(urlStr + 'category');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Category> lsCategorys = jsonItems
            .map<Category>((json) => Category.fromJson(json))
            .toList();

        await deleteAllCategorys();
        await insertUpdateCategorys(lsCategorys);
      } else {
        throw Exception('Failed to load Categorys');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}