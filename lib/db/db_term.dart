// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class TermDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Term>> allTerms() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(terms)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertTerm(Term term) {
    return into(terms).insert( term );
  }

  insertUpdateTerms(List<Term> lsTerm) async {
    // lsTerm.forEach( (Term term) => into(terms).insertOnConflictUpdate( term ) );
    for (Term term in lsTerm) {
      await into(terms).insertOnConflictUpdate( term );
    }
    return;
  }

  Future deleteAllTerms() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(terms)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getTerms() async {
    try {
      var url = Uri.parse(urlStr + 'terms');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Term> lsTerms = jsonItems
            .map<Term>((json) => Term.fromJson(json))
            .toList();

        await deleteAllTerms();
        await insertUpdateTerms(lsTerms);
      } else {
        throw Exception('Failed to load Terms');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}