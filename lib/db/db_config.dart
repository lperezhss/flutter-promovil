// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'package:drift/drift.dart';
import 'package:promovil/db/db_constant.dart';

import 'database.dart';

class ConfigDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Config>> allConfigs() async {
    return (select(configs) ).get();
  }

  Future<Config> getActiveCoupon() {
    return (select(configs)..where((tbl) => tbl.is_active.equals('S'))).getSingle();
  }

  Future<String> getCoupon() async {
    Config config = await getActiveCoupon();
    return config.coupon;
  }

  Future insertConfig(Config config) {
    return into(configs).insert( config );
  }

  insertUpdateConfigs(List<Config> lsConfig) async {
    // lsConfig.forEach( (ConfigsCompanion config) => into(configs).insertOnConflictUpdate( config ) );
    for (Config config in lsConfig) {
      await into(configs).insertOnConflictUpdate( config );
    }
    return;
  }

  Future deleteAllConfigs() {
    // delete the oldest nine tasks
    return ( delete(configs)..where( (tbl) => tbl.coupon.isNotNull() ) ).go();
  }

  Future<String> getURL() async {
    Config config = await getActiveCoupon();
    
    return config.protocol + '://' + config.ip + (config.port == '' ? '' : ':' + config.port) + '/';
  }

  Future verifyConfig() async {
    List<Config> lsConfig = await allConfigs();

    if (lsConfig.isEmpty) {
      var constant = ConstantDB().toMap();
      Config config = Config.fromJson(constant);
      await into(configs).insertOnConflictUpdate( config );
    }
  }
}