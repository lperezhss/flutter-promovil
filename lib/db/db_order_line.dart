import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;

import 'database.dart';
// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'db_config.dart';

class OrderLineDB extends Database {

  static const String finish = 'FINISH';

  Future<List<OrderLine>> allOrderLines() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(orderLines)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future<List<OrderLine>> getLinesByOrder(String order_id) async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(orderLines)..where( (tbl) => tbl.order_id.equals(order_id) & tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future insertOrderLine(OrderLine orderLine) {
    return into(orderLines).insert( orderLine );
  }

  insertUpdateOrderLines(List<OrderLine> lsOrderLine) async {
    // lsOrderLine.forEach( (OrderLine orderLine) => into(orderLine).insertOnConflictUpdate( orderLine ) );
    for (OrderLine orderLine in lsOrderLine) {
      await into(orderLines).insertOnConflictUpdate( orderLine );
    }
    return;
  }

  Future deleteAllOrderLines() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(orderLines)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getOrderLines() async {
    try {
      var url = Uri.parse(urlStr + 'orderLine');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<OrderLine> lsOrderLines = jsonItems
            .map<OrderLine>((json) => OrderLine.fromJson(json))
            .toList();

        await deleteAllOrderLines();
        await insertUpdateOrderLines(lsOrderLines);
      } else {
        throw Exception('Failed to load OrderLines');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}