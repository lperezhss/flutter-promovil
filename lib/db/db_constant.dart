// ignore_for_file: non_constant_identifier_names, constant_identifier_names

class ConstantDB {
  static const String name = 'HSS Demo 2K8';
  static const String coupon = '0000-0000';
  static const String protocol = 'http';
  static const String ip = '192.168.100.80';
  static const String port = '1555';
  static const String vendor = '03';
  static const String tax = '16';
  static const String view_stock = 'S';
  static const String view_tax = 'S';
  static const String number = '0';
  static const String validate_stock = 'N';
  static const String validate_balance = 'N';
  static const String view_term = 'S';
  static const String is_active = 'S';

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'coupon': coupon,
      'protocol': protocol,
      'ip': ip,
      'port': port,
      'vendor': vendor,
      'tax': tax,
      'view_stock': view_stock,
      'view_tax': view_tax,
      'number': number,
      'validate_stock': validate_stock,
      'validate_balance': validate_balance,
      'view_term': view_term,
      'is_active': is_active,
    };
  }

  static String getURL() {
    return protocol + '://' + ip + (port == '' ? '' : ':' + port) + '/';
  }

}