// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:promovil/db/db_config.dart';

import 'database.dart';

class CustomerDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Customer>> allCustomers() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(customers)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future<Customer> getCustomer(String customer_id) async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(customers)..where( (tbl) => tbl.id.equals(customer_id) & tbl.coupon.equals(activeCoupon) ) ).getSingle();
  }

  Future insertCustomer(Customer customer) {
    return into(customers).insert( customer );
  }

  insertUpdateCustomers(List<Customer> lsCustomer) async {
    // lsCustomer.forEach( (Customers customer) => into(customers).insertOnConflictUpdate( customer ) );
    for (Customer customer in lsCustomer) {
      await into(customers).insertOnConflictUpdate( customer );
    }
    return;
  }

  Future deleteAllCustomers() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(customers)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }


  Future<String> getCustomers() async {
    try {
      var url = Uri.parse((await ConfigDB().getURL()) + 'customer');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Customer> lsCustomers = jsonItems
            .map<Customer>((json) => Customer.fromJson(json))
            .toList();

        await deleteAllCustomers();
        await insertUpdateCustomers(lsCustomers);
      } else {
        throw Exception('Failed to load Customers');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}