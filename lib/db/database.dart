// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'package:drift/drift.dart';
import 'package:drift_sqflite/drift_sqflite.dart';

part 'database.g.dart';

const urlCouponStr = 'http://192.168.100.80:1555/';
const urlStr = 'http://192.168.100.80:1555/';

@DataClassName('Config')
class Configs extends Table {
  TextColumn get name => text()();
  TextColumn get coupon => text()();
  TextColumn get protocol => text()();
  TextColumn get ip => text()();
  TextColumn get port => text()();
  TextColumn get vendor => text()();
  TextColumn get tax => text()();
  TextColumn get view_stock => text()();
  TextColumn get view_tax => text()();
  TextColumn get number => text()();
  TextColumn get validate_stock => text()();
  TextColumn get validate_balance => text()();
  TextColumn get view_term => text()();
  TextColumn get is_active => text()();

  @override
  Set<Column> get primaryKey => {coupon, coupon};
}

@DataClassName('Coupon')
class Coupons extends Table {
  TextColumn get name => text()();
  TextColumn get coupon => text()();
  TextColumn get ip => text()();
  TextColumn get protocol => text()();
  TextColumn get port => text()();
  TextColumn get vendor => text()();
  TextColumn get tax => text()();
  TextColumn get view_stock => text()();
  TextColumn get view_tax => text()();
  TextColumn get number => text()();
  TextColumn get validate_stock => text()();
  TextColumn get validate_balance => text()();
  TextColumn get view_term => text()();

  @override
  Set<Column> get primaryKey => {coupon};
}

@DataClassName('Customer')
class Customers extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get taxid => text()();
  TextColumn get phone => text()();
  TextColumn get address => text()();
  TextColumn get vendor => text()();
  TextColumn get zone => text()();
  TextColumn get email => text()();
  TextColumn get tprice => text()();
  TextColumn get route => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('Product')
class Products extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get category_id => text()();
  TextColumn get type => text()();
  TextColumn get taxtype=> text()();
  TextColumn get ref => text()();
  TextColumn get stock => text()();
  TextColumn get price1 => text()();
  TextColumn get price2 => text()();
  TextColumn get price3 => text()();
  TextColumn get price4 => text()();
  TextColumn get price5 => text()();
  TextColumn get uom1 => text()();
  TextColumn get uom2 => text()();
  TextColumn get equivalent1 => text()();
  TextColumn get equivalent2 => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('Document')
class Documents extends Table {
  TextColumn get customer_id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get taxid => text()();
  TextColumn get doctype => text()();
  TextColumn get docnum => text()();
  TextColumn get datetrx => text()();
  TextColumn get amount => text()();
  TextColumn get balance => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {customer_id, doctype, docnum, coupon};
}

@DataClassName('DocumentSummary')
class DocumentSummarys extends Table {
  TextColumn get customer_id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get taxid => text()();
  TextColumn get amount => text()();
  TextColumn get balance => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {customer_id, coupon};
}

@DataClassName('Category')
class Categorys extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('PaymentType')
class PaymentTypes extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('CashBank')
class CashBanks extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get type => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('Term')
class Terms extends Table {
  TextColumn get id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get days => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('Order')
class Orders extends Table {
  TextColumn get id => text()();
  TextColumn get docnum => text()();
  TextColumn get datetrx => text()();
  TextColumn get term => text()();
  TextColumn get description => text()();
  TextColumn get customer_id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get taxid => text()();
  TextColumn get vendor => text()();
  TextColumn get subtotal => text()();
  TextColumn get tax => text()();
  TextColumn get total => text()();
  TextColumn get status => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DataClassName('OrderLine')
class OrderLines extends Table {
  TextColumn get id => text()();
  TextColumn get order_id => text()();
  TextColumn get line => text()();
  TextColumn get product_id => text()();
  TextColumn get value => text()();
  TextColumn get name => text()();
  TextColumn get qty => text()();
  TextColumn get price => text()();
  TextColumn get price_entered => text()();
  TextColumn get uom => text()();
  TextColumn get equivalent => text()();
  TextColumn get tax => text()();
  TextColumn get total => text()();
  TextColumn get coupon => text()();

  @override
  Set<Column> get primaryKey => {id, coupon};
}

@DriftDatabase(
  tables: [
    Configs,
    Coupons,
    Customers,
    Products,
    Documents,
    DocumentSummarys,
    Categorys,
    PaymentTypes,
    CashBanks,
    Terms,
    Orders,
    OrderLines
  ],
  queries: {},
)

class Database extends _$Database {
  Database()
      : super(SqfliteQueryExecutor.inDatabaseFolder(
      path: 'promovil.db', logStatements: true));

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      onCreate: (Migrator m) {
        return m.createAll();
      },
      onUpgrade: (Migrator m, int from, int to) async {
        /*if (from == 2) {
          await m.createTable(products);
        }*/
      },
      beforeOpen: (details) async {
        if (details.wasCreated) {
        }
      },
    );
  }
}