// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;

import 'database.dart';
import 'db_config.dart';

class OrderDB extends Database {

  static const String finish = 'FINISH';

  Future<List<Order>> allOrders() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(orders)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).get();
  }

  Future<Order> getOrder(String order_id) async {
    String activeCoupon = await ConfigDB().getCoupon();
    return (select(orders)..where( (tbl) => tbl.id.equals(order_id) & tbl.coupon.equals(activeCoupon) ) ).getSingle();
  }

  Future insertOrder(Order order) {
    return into(orders).insert( order );
  }

  insertUpdateOrders(List<Order> lsOrders) async {
    // orders.forEach( (Order order) => into(order).insertOnConflictUpdate( order ) );
    for (Order order in lsOrders) {
      await into(orders).insertOnConflictUpdate( order );
    }
    return;
  }

  Future deleteAllOrders() async {
    String activeCoupon = await ConfigDB().getCoupon();
    return ( delete(orders)..where( (tbl) => tbl.coupon.equals(activeCoupon) ) ).go();
  }

  Future<List<Order>> getOrderComplete(String order_id) async {
    return select(orders).get();
  }



  Future<String> getOrders() async {
    try {
      var url = Uri.parse(urlStr + 'order');

      final response = await http.get(url);
      if (response.statusCode == 200) {
        var jsonItems = jsonDecode(response.body);

        List<Order> lsOrders = jsonItems
            .map<Order>((json) => Order.fromJson(json))
            .toList();

        await deleteAllOrders();
        await insertUpdateOrders(lsOrders);
      } else {
        throw Exception('Failed to load Orders');
      }
    } on Exception catch (exception) {
      return exception.toString();
    } catch (error) {
      return error.toString();
    }

    return finish;
  }
}