import 'package:flutter/material.dart';

class Choice {
  const Choice({required this.title, required this.icon, required this.routeName});
  final String title;
  final IconData icon;
  final String routeName;
}

class MenuCard extends StatelessWidget {
  const MenuCard({Key? key, required this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(15),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Icon(
                      choice.icon,
                      size:80.0,
                      color: Colors.blueGrey,
                    )
                ),
                Text(choice.title,
                    style: const TextStyle(
                      fontSize: 25,
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ]
          ),
        ),
        splashColor: Colors.black45,
        onTap: () {
          Navigator.pushNamed(context, choice.routeName);
        },
      ),
    );
  }
}