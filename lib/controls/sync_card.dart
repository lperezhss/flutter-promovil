import 'package:flutter/material.dart';

import '../db/database.dart';
import '../db/db_cash_bank.dart';
import '../db/db_category.dart';
import '../db/db_coupon.dart';
import '../db/db_customer.dart';
import '../db/db_document.dart';
import '../db/db_document_summary.dart';
import '../db/db_payment_types.dart';
import '../db/db_product.dart';
import '../db/db_term.dart';

final db = Database();
final couponDB = CouponDB();
final categoryDB = CategoryDB();
final productDB = ProductDB();
final customerDB = CustomerDB();
final documentDB = DocumentDB();
final documentSummaryDB = DocumentSummaryDB();
final cashBankDB = CashBankDB();
final termDB = TermDB();
final paymentTypeDB = PaymentTypeDB();

class Choice {
  const Choice({required this.title, required this.icon, required this.valueCheck});
  final String title;
  final IconData icon;
  final bool valueCheck;
}

const List<Choice> choices = <Choice>[
  Choice(title: SyncCard.menuNameCoupon, icon: Icons.qr_code, valueCheck: true),
  Choice(title: SyncCard.menuNameCustomer, icon: Icons.person_outline, valueCheck: true),
  Choice(title: SyncCard.menuNameProduct, icon: Icons.sticky_note_2_outlined, valueCheck: true),
  Choice(title: SyncCard.menuNameDocument, icon: Icons.file_present_outlined, valueCheck: true),
  Choice(title: SyncCard.menuNamePayment, icon: Icons.payment_outlined, valueCheck: true),
  Choice(title: SyncCard.menuNameStatistics, icon: Icons.bar_chart_outlined, valueCheck: false),
  Choice(title: SyncCard.menuNameOrder, icon: Icons.upload_file_outlined, valueCheck: false),
  Choice(title: SyncCard.menuNameImage, icon: Icons.image_outlined, valueCheck: false),
];

class SyncCard extends StatefulWidget {
  const SyncCard({Key? key, required this.choice, required this.inProgress, required this.onChanged}) : super(key: key);
  final ValueChanged<bool> onChanged;
  final Choice choice;
  final bool inProgress;

  static const String menuNameCoupon = 'Cupon';
  static const String menuNameCustomer = 'Clientes';
  static const String menuNameProduct = 'Articulos';
  static const String menuNameDocument = 'Documentos';
  static const String menuNamePayment = 'Cobros';
  static const String menuNameStatistics = 'Estadistica';
  static const String menuNameOrder = 'Pedidos';
  static const String menuNameImage = 'Imagenes';

  static const String finish = 'FINISH';


  @override
  _SyncCardState createState() => _SyncCardState();
}

class _SyncCardState extends State<SyncCard> {
  late final String _title = widget.choice.title;
  late final IconData _iconData = widget.choice.icon;
  late bool _valueCheck = widget.choice.valueCheck;
  late var _inProgress = widget.inProgress;
  late bool _inProgressItem = false;
  late String _isError = '';

  void _changeProgressItemsSate(bool value) {
    widget.onChanged(value);
  }

  void _inProgressState() {
    setState(() {
      _inProgress = widget.inProgress;
      if (_valueCheck && _inProgress) {
        _changeProgressItemsSate(false);
        _inProgressItem = true;
        _isError = '';
      }
    });
  }

  void _valueCheckState() {
    setState(() {
      if (!_inProgress) {
        if (_title != SyncCard.menuNameCoupon) {
          _valueCheck = !_valueCheck;
          _changeProgressItemsSate(!_valueCheck);
        } else if (!_valueCheck) {
          _valueCheck = !_valueCheck;
        }
      }
    });
  }

  Future<bool> _inProgressItemState() async {
    switch(widget.choice.title) {
      case SyncCard.menuNameCoupon: {
        _finisProcess(await couponDB.getCoupons());
      }
      break;

      case SyncCard.menuNameCustomer: {
        _finisProcess(await customerDB.getCustomers());
      }
      break;

      case SyncCard.menuNameProduct: {
        await categoryDB.getCategorys();
        _finisProcess(await productDB.getProducts());
      }
      break;

      case SyncCard.menuNameDocument: {
        await documentSummaryDB.getDocumentSummarys();
        _finisProcess(await documentDB.getDocuments());
      }
      break;

      case SyncCard.menuNamePayment: {
        await termDB.getTerms();
        await cashBankDB.getCashBanks();
        _finisProcess(await paymentTypeDB.getPaymentTypes());
      }
      break;

      case SyncCard.menuNameStatistics: {
        _finisProcess(SyncCard.finish);
      }
      break;

      case SyncCard.menuNameOrder: {
        _finisProcess(SyncCard.finish);
      }
      break;

      case SyncCard.menuNameImage: {
        _finisProcess(SyncCard.finish);
      }
      break;
    }

    _inProgressState();
    return true;
  }

  _finisProcess(String result) {
    _isError = result != SyncCard.finish ? result : '';
    _inProgressItem = false;
    _valueCheck = false;
    _changeProgressItemsSate(true);
  }

  LinearProgressIndicator _valueProgressState() {
    if (_valueCheck && _inProgress && _inProgressItem) {
      _inProgressItemState();
      return const LinearProgressIndicator(
        minHeight: 15,
        backgroundColor: Colors.black12,
        color: Colors.blueGrey,
      );
    } else {
      return const LinearProgressIndicator(
        value: 0.0,
        minHeight: 15,
        backgroundColor: Colors.black12,
        color: Colors.blueGrey,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    _inProgressState();
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(5),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Container(
          margin: const EdgeInsets.all(10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.topCenter,
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 15),
                  child: Column(
                    children: [
                      Icon(
                        _iconData,
                        size:50.0,
                        color: Colors.blueGrey,
                      ),
                      Text(_title,
                          style: const TextStyle(
                            fontSize: 15,
                            color: Colors.blueGrey,
                            fontWeight: FontWeight.bold,
                          )
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: _valueProgressState(),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Switch(
                    value: _valueCheck,
                    onChanged: (value) {
                      _valueCheckState();
                    },
                    activeColor: Colors.blueGrey,
                    activeTrackColor: Colors.black12,
                  ),
                ),
              ]
          ),
        ),
        splashColor: Colors.blueGrey,
        onTap: () async {
          _valueCheckState();
        },
      ),
    );
  }
}