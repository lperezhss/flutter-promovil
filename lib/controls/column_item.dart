import 'package:flutter/material.dart';

class ColumnsItemSingle extends StatelessWidget {
  const ColumnsItemSingle({Key? key, required this.label, required this.value, required this.resalted}) : super(key: key);
  final String label;
  final String value;
  final bool resalted;

  double _sizeValue() {
    if (resalted) {
      return 18;
    } else {
      return 13;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ColumnsItemDetail(
          label: label,
          value: value,
          sizeValue: _sizeValue(),
        ),
      ],
    );
  }
}

class ColumnsItemDuoble extends StatelessWidget {
  const ColumnsItemDuoble({Key? key, required this.label1, required this.value1, required this.label2, required this.value2}) : super(key: key);
  final String label1;
  final String value1;
  final String label2;
  final String value2;

  double _size() {
    return 13;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ColumnsItemDetail(
          label: label1,
          value: value1,
          sizeValue: _size(),
        ),
        ColumnsItemDetail(
          label: label2,
          value: value2,
          sizeValue: _size(),
        ),
      ],
    );
  }
}

class ColumnsItemDetail extends StatelessWidget {
  const ColumnsItemDetail({Key? key, required this.label, required this.value, required this.sizeValue}) : super(key: key);
  final String label;
  final String value;
  final double sizeValue;

  TextStyle styleLabel() {
    double _size = 13;

    return TextStyle(
      fontSize: _size,
      color: Colors.blueGrey,
      fontWeight: FontWeight.normal,
    );
  }

  TextStyle styleValue() {
    double _size = sizeValue;

    return TextStyle(
      fontSize: _size,
      color: Colors.blueGrey,
      fontWeight: FontWeight.bold,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
          margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Text(label,
                      textAlign: TextAlign.right,
                      style: styleLabel(),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(label == '' ? '' : ':',
                      textAlign: TextAlign.center,
                      style: styleLabel(),
                  ),
                ),
                Expanded(
                    flex: 6,
                    child: Text(value,
                        style: styleValue(),
                    ),
                ),
              ]
          )
      ),
    );
  }
}