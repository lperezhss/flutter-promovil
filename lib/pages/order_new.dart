import 'package:flutter/material.dart';
import  'package:intl/intl.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_customer.dart';

Future<List<Customer>> getList() async {
  return CustomerDB().allCustomers();
}

class OrderNew extends StatefulWidget {
  const OrderNew({Key? key}) : super(key: key);

  @override
  _OrderNewState createState() => _OrderNewState();
}

class _OrderNewState extends State<OrderNew> {
  late var _customer = null;

  String cdate = DateFormat("dd-MM-yyyy").format(DateTime.now());

  @override
  void initState() {
    super.initState();
  }

  setCustomer(Customer value) {
    setState(() {
      _customer = value;
    });
  }

  Widget getCustomer1() {
    if (_customer == null) {
      return const ColumnsItemDuoble(
          label1: 'Codigo',
          value1: '',
          label2: 'Nombre',
          value2: ''
      );
    } else {
      Customer _customer1 = _customer;
      return ColumnsItemDuoble(
          label1: 'Codigo',
          value1: _customer1.value.toUpperCase(),
          label2: 'Nombre',
          value2: _customer1.name.toUpperCase(),
      );
    }
  }

  Widget getCustomer2() {
    if (_customer == null) {
      return const ColumnsItemDuoble(
          label1: 'Telefono',
          value1: '',
          label2: 'EMail',
          value2: ''
      );
    } else {
      Customer _customer1 = _customer;
      return ColumnsItemDuoble(
          label1: 'Telefono',
          value1: _customer1.phone,
          label2: 'EMail',
          value2: _customer1.email,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('ProMovil - Crear Orden')),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Divider(
              color: Colors.white,
            ),
            const Text('Datos del Pedido',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                )
            ),
            ColumnsItemDuoble(
              label1: 'Fecha',
              value1: cdate,
              label2: '',
              value2: ''
            ),
            const Divider(
              color: Colors.white,
            ),
            const Text('Cliente',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                )
            ),
            getCustomer1(),
            getCustomer2(),
            const Divider(
              color: Colors.white,
            ),
            ElevatedButton(
              child: const Text('Seleccionar Cliente'),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => FutureBuilder<List<Customer>>(
                    future: getList(),
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return const Center(
                          child: Text('An error has occurred!'),
                        );
                      } else if (snapshot.hasData) {
                        return TaskList(
                          customers: snapshot.data!,
                          onChanged: (Customer value) {
                            setCustomer(value);
                          },
                        );
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                );
              },
            ),
            const Divider(
              color: Colors.blueGrey,
            ),
            const Text('Articulos',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                )
            ),
            const Divider(
              color: Colors.white,
            ),
            ElevatedButton(
              child: const Text('Incluir / Modificar'),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => FutureBuilder<List<Customer>>(
                    future: getList(),
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return const Center(
                          child: Text('An error has occurred!'),
                        );
                      } else if (snapshot.hasData) {
                        return TaskList(
                          customers: snapshot.data!,
                          onChanged: (Customer value) {
                            setCustomer(value);
                          },
                        );
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}



class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.customers, required this.onChanged}) : super(key: key);

  final List<Customer> customers;
  final ValueChanged<Customer> onChanged;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // padding: const EdgeInsets.all(5),
        itemCount: customers.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                customer: customers[index],
                onChanged: (Customer value) {
                  onChanged(value);
                },
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.customer, required this.onChanged}) : super(key: key);
  final Customer customer;
  final ValueChanged<Customer> onChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ColumnsItemSingle(
                    label: 'Código',
                    value: customer.value.toUpperCase(),
                    resalted: true,
                  ),
                  ColumnsItemSingle(
                    label: 'Nombre',
                    value: customer.name.toUpperCase(),
                    resalted: true,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  ColumnsItemSingle(
                    label: 'Dirección',
                    value: customer.address.toUpperCase(),
                    resalted: false,
                  ),
                  const Divider(
                    color: Colors.white,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Ident.',
                    value1: customer.taxid,
                    label2: 'D.Ruta',
                    value2: customer.route,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Telf.',
                    value1: customer.phone,
                    label2: 'Zona',
                    value2: customer.zone,
                  ),
                  ColumnsItemDuoble(
                    label1: 'EMail',
                    value1: customer.email,
                    label2: 'Tipo',
                    value2: customer.tprice,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton.icon(
                      icon: const Icon(Icons.payment_outlined, size: 18),
                      label: const Text('SALDO - \$ 123.45'),
                      onPressed: () {
                        Navigator.of(context).pop();
                        onChanged(customer);
                      },
                    ),
                  ),
                ]
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
        },
      ),
    );
  }
}