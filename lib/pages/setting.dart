import 'package:flutter/material.dart';
import 'package:promovil/controls/menu_card.dart';

import '../route/route.dart' as route;

class Setting extends StatelessWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Configuración')),
        body: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: List.generate(choices.length, (index) {
              return Center(
                child: MenuCard(choice: choices[index]),
              );
            })
        )
    );
  }
}

const List<Choice> choices = <Choice>[
  Choice(title: 'En desarrollo', icon: Icons.person_outline, routeName: route.emptyPage),
];