import 'package:flutter/material.dart';

class AboutContact extends StatelessWidget {
  const AboutContact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('ProMovil - Contactenos')),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: const <Widget>[
              /*Text('Desarrollador',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              Divider(
                color: Colors.white,
              ),
              SelectCardDev(
                contact: 'Ing. Luis Pérez',
                phone: '+593-96-0214102',
                email: 'lperez.hss@gmail.com',
                location: 'Quito - Ecuador',
              ),
              Divider(
                color: Colors.white,
              ),
              Divider(
                color: Colors.blueGrey,
              ),
              Text('Distribuidores Autorizados',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              Divider(
                color: Colors.white,
              ),
              SelectCard(
                contact: 'Joseph Volcan',
                company: 'SAECA Consultores 2020, C.A.',
                taxID: 'J-31662241-0',
                phone: '+58-412-9550259',
                email: 'saecaconsultores@gmail.com',
                location: 'Caracas - Venezuela',
              ),*/
              SelectCard(
                contact: 'Jonas Ayala',
                company: 'SGO SOLUGER C.A.',
                taxID: 'J-29671902-0',
                phone: '+58-414-3286898',
                email: 'info@sgsoluger.com',
                location: 'Caracas - Venezuela',
              ),
            ]
        ),
      ),
    );
  }
}

class SelectCardDev extends StatelessWidget {
  const SelectCardDev({Key? key, required this.email, required this.phone, required this.location, required this.contact}) : super(key: key);
  final String contact;
  final String email;
  final String phone;
  final String location;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(15),
      elevation: 10,
      color: Colors.white,
      child: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Divider(
                color: Colors.white,
              ),
              Text(contact,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              const Divider(
                color: Colors.blueGrey,
              ),
              Text('EMail: ' + email,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('Telefono: ' + phone,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('Ubicación: ' + location,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              const Divider(
                color: Colors.white,
              ),
            ]
        ),
      ),
    );
  }
}

class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.taxID, required this.company, required this.email, required this.phone, required this.location, required this.contact}) : super(key: key);
  final String taxID;
  final String company;
  final String contact;
  final String email;
  final String phone;
  final String location;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(15),
      elevation: 10,
      color: Colors.white,
      child: Center(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Divider(
                color: Colors.white,
              ),
              Text(company,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              const Divider(
                color: Colors.blueGrey,
              ),
              Text('RIF: ' + taxID,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('Contacto: ' + contact,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('EMail: ' + email,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('Telefono: ' + phone,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              Text('Ubicación: ' + location,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.normal,
                  )
              ),
              const Divider(
                color: Colors.white,
              ),
            ]
        ),
      ),
    );
  }
}