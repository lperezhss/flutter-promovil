import 'package:flutter/material.dart';
import 'package:promovil/controls/menu_card.dart';

import '../route/route.dart' as route;

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Menú')),
        body: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: List.generate(choices.length, (index) {
              return Center(
                child: MenuCard(choice: choices[index]),
              );
            })
        )
    );
  }
}

const List<Choice> choices = <Choice>[
  Choice(title: 'Consultar', icon: Icons.search_outlined, routeName: route.consultPage),
  Choice(title: 'Pedidos', icon: Icons.shopping_cart_outlined, routeName: route.orderPage),
  Choice(title: 'Sincronizar', icon: Icons.sync_outlined, routeName: route.syncPage),
  Choice(title: 'Config.', icon: Icons.settings_outlined, routeName: route.settingPage),
  Choice(title: 'Acerca de...', icon: Icons.lightbulb_outline, routeName: route.aboutPage),
  Choice(title: 'Salir', icon: Icons.power_settings_new_outlined, routeName: route.exitPage),
];