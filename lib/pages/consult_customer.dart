import 'package:flutter/material.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_customer.dart';
import '../route/route.dart' as route;

Future<List<Customer>> getList() async {
  return CustomerDB().allCustomers();
}

class ConsultCustomer extends StatelessWidget {
  const ConsultCustomer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Consultar Clientes')),
        body: FutureBuilder<List<Customer>>(
          future: getList(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Text('An error has occurred!'),
              );
            } else if (snapshot.hasData) {
              return TaskList(customers: snapshot.data!);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.customers}) : super(key: key);

  final List<Customer> customers;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // padding: const EdgeInsets.all(5),
        itemCount: customers.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                customer: customers[index],
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.customer}) : super(key: key);
  final Customer customer;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ColumnsItemSingle(
                    label: 'Código',
                    value: customer.value.toUpperCase(),
                    resalted: true,
                  ),
                  ColumnsItemSingle(
                    label: 'Nombre',
                    value: customer.name.toUpperCase(),
                    resalted: true,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  ColumnsItemSingle(
                    label: 'Dirección',
                    value: customer.address.toUpperCase(),
                    resalted: false,
                  ),
                  const Divider(
                    color: Colors.white,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Ident.',
                    value1: customer.taxid,
                    label2: 'D.Ruta',
                    value2: customer.route,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Telf.',
                    value1: customer.phone,
                    label2: 'Zona',
                    value2: customer.zone,
                  ),
                  ColumnsItemDuoble(
                    label1: 'EMail',
                    value1: customer.email,
                    label2: 'Tipo',
                    value2: customer.tprice,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton.icon(
                      icon: const Icon(Icons.payment_outlined, size: 18),
                      label: const Text('SALDO - \$ 123.45'),
                      onPressed: () {
                        Navigator.pushNamed(
                          context,
                          route.consultDocumentPage,
                          arguments: customer.id,
                        );
                      },
                    ),
                  ),
                ]
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
        },
      ),
    );
  }
}