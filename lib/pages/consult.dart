import 'package:flutter/material.dart';

import '../controls/menu_card.dart';
import '../route/route.dart' as route;

class Consult extends StatelessWidget {
  const Consult({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Consultar')),
        body: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: List.generate(choices.length, (index) {
              return Center(
                child: MenuCard(choice: choices[index]),
              );
            })
        )
    );
  }
}

const List<Choice> choices = <Choice>[
  Choice(title: 'Clientes', icon: Icons.person_outline, routeName: route.consultCustomerPage),
  Choice(title: 'Articulos', icon: Icons.sticky_note_2_outlined, routeName: route.consultProductPage),
  Choice(title: 'Documentos', icon: Icons.file_present_outlined, routeName: route.consultDocumentSummaryPage),
  Choice(title: 'Estadistica', icon: Icons.bar_chart_outlined, routeName: route.consultDocumentPage),
  Choice(title: 'Ruta de Venta', icon: Icons.event_available_outlined, routeName: route.emptyPage),
  Choice(title: 'Promociones', icon: Icons.mail_outline, routeName: route.emptyPage),
];