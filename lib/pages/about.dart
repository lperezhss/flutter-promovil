import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../route/route.dart' as route;

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  _MyAboutPageState createState() => _MyAboutPageState();
}

class _MyAboutPageState extends State<About> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('ProMovil - Acerca de...'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Image(
                image: AssetImage('assets/images/logo.png'),
                width: 200,
                height: 200,
              ),
              const Divider(
                color: Colors.blueGrey,
              ),
              const Text('ProMovil es una herramienta desarrollada en función de la movilidad de las empresas, en permitir al personal contar con una herramienta que derribe los límites de las distancias en zonas foráneas a la empresa.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              const Divider(
                color: Colors.white,
              ),
              Text('ProMovil: ' + _packageInfo.version,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              Text('ID: ' + _packageInfo.buildSignature,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  )
              ),
              const Divider(
                color: Colors.white,
              ),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton.icon(
                  icon: const Icon(Icons.phone, size: 18),
                  label: const Text('CONTACTENOS'),
                  onPressed: () {
                    Navigator.pushNamed(context, route.aboutContactPage);
                  },
                ),
              ),
            ],
          ),
        )
    );
  }
}