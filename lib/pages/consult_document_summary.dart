import 'package:flutter/material.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_document_summary.dart';
import '../route/route.dart' as route;

Future<List<DocumentSummary>> getList() async {
  return DocumentSummaryDB().allDocumentSummarys();
}

class ConsultDocumentSummary extends StatelessWidget {
  const ConsultDocumentSummary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Consultar Documentos')),
        body: FutureBuilder<List<DocumentSummary>>(
          future: getList(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Text('An error has occurred!'),
              );
            } else if (snapshot.hasData) {
              return TaskList(documentSummarys: snapshot.data!);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.documentSummarys}) : super(key: key);

  final List<DocumentSummary> documentSummarys;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // padding: const EdgeInsets.all(5),
        itemCount: documentSummarys.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                documentSummary: documentSummarys[index],
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.documentSummary}) : super(key: key);
  final DocumentSummary documentSummary;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ColumnsItemSingle(
                    label: 'Cliente',
                    value: documentSummary.name.toUpperCase(),
                    resalted: true,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Código',
                    value1: documentSummary.value.toUpperCase(),
                    label2: 'Ident.',
                    value2: documentSummary.taxid,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Total',
                    value1: documentSummary.amount,
                    label2: 'Saldo',
                    value2: documentSummary.balance,
                  ),
                  const Text('',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.normal,
                      )
                  ),
                ]
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
          Navigator.pushNamed(
            context,
            route.consultDocumentPage,
            arguments: documentSummary.customer_id,
          );
        },
      ),
    );
  }
}