import 'package:flutter/material.dart';

import '../controls/sync_card.dart';

class Sync extends StatefulWidget {
  const Sync({Key? key}) : super(key: key);

  @override
  _SyncState createState() => _SyncState();
}

class _SyncState extends State<Sync> {
  late bool _inProgress = false;

  late bool _finCoupon = true;
  late bool _finCustomers = true;
  late bool _finProducts = true;
  late bool _finDocuments = true;
  late bool _finPayment = true;
  late bool _finStatistics = true;
  late bool _finOrder = true;
  late bool _finImage = true;

  void _inProgressSate(bool inProgress) {
    setState(() {
      _inProgress = inProgress;
    });
  }

  void _changeProgressSate(String item, bool value) {
    switch(item) {
      case SyncCard.menuNameCoupon: {
        _finCoupon = value;
      }
      break;

      case SyncCard.menuNameCustomer: {
        _finCustomers = value;
      }
      break;

      case SyncCard.menuNameProduct: {
        _finProducts = value;
      }
      break;

      case SyncCard.menuNameDocument: {
        _finDocuments = value;
      }
      break;

      case SyncCard.menuNamePayment: {
        _finPayment = value;
      }
      break;

      case SyncCard.menuNameStatistics: {
        _finStatistics = true;
      }
      break;

      case SyncCard.menuNameOrder: {
        _finOrder = true;
      }
      break;

      case SyncCard.menuNameImage: {
        _finImage = true;
      }
      break;
    }

    if (_inProgress) {
      if (_finCustomers &&
          _finProducts &&
          _finDocuments &&
          _finPayment &&
          _finCoupon &&
          _finStatistics &&
          _finOrder && _finImage) {

        _inProgressSate(false);
      }
    }
  }

  Text _inProgressSatte2() {
    return Text(!_inProgress ? 'SINCRONIZAR' : 'SINCRONIZANDO');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('ProMovil - Sincronizar'),
        ),
        body: Column(
          children: [
            Center(
              child: Container(
                margin: const EdgeInsets.all(20),
                child: ElevatedButton.icon(
                  label: _inProgressSatte2(),
                  icon: const Icon(Icons.sync_outlined),
                  onPressed: () {
                    _inProgressSate(true);
                  },
                ),
              )
            ),
            const Divider(
              color: Colors.blueGrey,
            ),
            Expanded(
              child: GridView.count(
                  shrinkWrap: true,
                  primary: true,
                  crossAxisCount: 2,
                  crossAxisSpacing: 4.0,
                  mainAxisSpacing: 8.0,
                  children: List.generate(choices.length, (index) {
                    return Center(
                      child: SyncCard(
                          choice: choices[index],
                          inProgress: _inProgress,
                          onChanged:  (value) {
                            _changeProgressSate(choices[index].title, value);
                          },
                      ),
                    );
                  })
              ),
            ),
          ],
        ),
    );
  }
}