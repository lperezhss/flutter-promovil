import 'package:flutter/material.dart';
import '../controls/menu_card.dart';

import '../route/route.dart' as route;

class Order extends StatelessWidget {
  const Order({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Ordenes')),
        body: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: List.generate(choices.length, (index) {
              return Center(
                child: MenuCard(choice: choices[index]),
              );
            })
        )
    );
  }
}

const List<Choice> choices = <Choice>[
  Choice(title: 'Nuevo', icon: Icons.insert_drive_file_outlined, routeName: route.orderNewPage),
  Choice(title: 'Consultar', icon: Icons.upload_file_outlined, routeName: route.orderConsultPage),
  Choice(title: 'Documentos', icon: Icons.file_present_outlined, routeName: route.consultDocumentSummaryPage),
  Choice(title: 'Estadistica', icon: Icons.bar_chart_outlined, routeName: route.emptyPage),
  Choice(title: 'Cobros', icon: Icons.payment_outlined, routeName: route.emptyPage),
  Choice(title: 'Reg. Cliente', icon: Icons.person_add_outlined, routeName: route.emptyPage),
];