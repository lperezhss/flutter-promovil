import 'package:flutter/material.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_product.dart';

Future<List<Product>> getList() async {
  return ProductDB().allProducts();
}

class ConsultProduct extends StatelessWidget {
  const ConsultProduct({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Consultar Productos')),
        body: FutureBuilder<List<Product>>(
          future: getList(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Text('An error has occurred!'),
              );
            } else if (snapshot.hasData) {
              return TaskList(products: snapshot.data!);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.products}) : super(key: key);

  final List<Product> products;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        // padding: const EdgeInsets.all(5),
        itemCount: products.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                product: products[index],
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.product}) : super(key: key);
  final Product product;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [
                      const Expanded(
                        flex: 3,
                        child: Image(
                          image: AssetImage('assets/images/logo.png'),
                          width: 75,
                          height: 75,
                        ),
                      ),
                      Expanded(
                        flex: 7,
                        child: Column(
                          children: [
                            Text(product.value.toUpperCase(),
                              textAlign: TextAlign.left,
                              style: const TextStyle(
                                fontSize: 15,
                                color: Colors.blueGrey,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const Divider(
                              color: Colors.white,
                            ),
                            Text(product.name.toUpperCase(),
                              textAlign: TextAlign.left,
                              style: const TextStyle(
                                fontSize: 15,
                                color: Colors.blueGrey,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        )
                      ),
                    ],
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Tipo',
                    value1: product.type,
                    label2: 'Imp.',
                    value2: product.taxtype,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Precio 1',
                    value1: product.price1.toString(),
                    label2: 'Stock',
                    value2: product.stock.toString(),
                  ),
                  ColumnsItemDuoble(
                    label1: 'Precio 2',
                    value1: product.price2.toString(),
                    label2: 'Precio 3',
                    value2: product.price3.toString(),
                  ),
                  ColumnsItemDuoble(
                    label1: 'Precio 4',
                    value1: product.price4.toString(),
                    label2: 'Precio 5',
                    value2: product.price5.toString(),
                  ),
                  ColumnsItemDuoble(
                    label1: 'Unid. 1',
                    value1: product.uom1,
                    label2: 'Unid. 2',
                    value2: product.uom2,
                  ),
                ]
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
        },
      ),
    );
  }
}