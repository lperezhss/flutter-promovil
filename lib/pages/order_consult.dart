import 'package:flutter/material.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_order.dart';

Future<List<Order>> getList() async {
  return OrderDB().allOrders();
}

class OrderConsult extends StatelessWidget {
  const OrderConsult({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Consultar Ordenes')),
        body: FutureBuilder<List<Order>>(
          future: getList(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return const Center(
                child: Text('An error has occurred!'),
              );
            } else if (snapshot.hasData) {
              return TaskList(orders: snapshot.data!);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.orders}) : super(key: key);

  final List<Order> orders;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // padding: const EdgeInsets.all(5),
        itemCount: orders.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                order: orders[index],
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.order}) : super(key: key);
  final Order order;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ColumnsItemSingle(
                    label: 'Código',
                    value: order.value.toUpperCase(),
                    resalted: true,
                  ),
                  ColumnsItemSingle(
                    label: 'Nombre',
                    value: order.name.toUpperCase(),
                    resalted: true,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                  const Divider(
                    color: Colors.white,
                  ),
                  ColumnsItemDuoble(
                    label1: 'Ident.',
                    value1: order.taxid,
                    label2: 'DocNum',
                    value2: order.docnum,
                  ),
                  ColumnsItemDuoble(
                    label1: 'SubTotal',
                    value1: order.subtotal,
                    label2: 'Total',
                    value2: order.total,
                  ),
                  const Divider(
                    color: Colors.blueGrey,
                  ),
                ]
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
        },
      ),
    );
  }
}