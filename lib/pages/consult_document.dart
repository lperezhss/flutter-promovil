import 'package:flutter/material.dart';

import '../controls/column_item.dart';
import '../db/database.dart';
import '../db/db_document.dart';

late String _customerId = "";

Future<List<Document>> getList() async {
  if (_customerId == '') {
    return DocumentDB().allDocuments();
  } else {
    return DocumentDB().getDocumentByCustomers(_customerId);
  }
}

class ConsultDocument extends StatefulWidget {
  const ConsultDocument({Key? key}) : super(key: key);

  @override
  _SyncState createState() => _SyncState();
}

class _SyncState extends State<ConsultDocument> {

  void _setCustomerId(String customerId) {
    setState(() {
      _customerId = customerId;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context)!.settings.arguments != null) {
      String argCustomerId = ModalRoute.of(context)!.settings.arguments as String;

      _setCustomerId(argCustomerId);
    } else {
      _setCustomerId("");
    }

    return Scaffold(
      appBar: AppBar(title: const Text('ProMovil - Consultar Documentos')),
      body: FutureBuilder<List<Document>>(
        future: getList(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            return TaskList(documents: snapshot.data!);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}

class TaskList extends StatelessWidget {
  const TaskList({Key? key, required this.documents}) : super(key: key);

  final List<Document> documents;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      // padding: const EdgeInsets.all(5),
        itemCount: documents.length,
        itemBuilder: (BuildContext context, int index) {
          return Center(
              child: SelectCard(
                document: documents[index],
              )
          );
        }
    );
  }
}


class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.document}) : super(key: key);
  final Document document;

  List<Widget> _getData() {
    if (_customerId == '') {
      return [
        ColumnsItemSingle(
          label: 'Cliente',
          value: document.name.toUpperCase(),
          resalted: true,
        ),
        const Divider(
          color: Colors.blueGrey,
        ),
        ColumnsItemDuoble(
          label1: 'Código',
          value1: document.value.toUpperCase(),
          label2: 'Ident.',
          value2: document.taxid,
        ),
        ColumnsItemDuoble(
          label1: document.doctype.substring(0, 1).toUpperCase() + document.doctype.substring(1).toLowerCase() + '.',
          value1: document.docnum,
          label2: 'Fecha.',
          value2: document.datetrx,
        ),
        ColumnsItemDuoble(
          label1: 'Total',
          value1: document.amount,
          label2: 'Saldo',
          value2: document.balance,
        ),
        const Text('',
            style: TextStyle(
              fontSize: 15,
              color: Colors.blueGrey,
              fontWeight: FontWeight.normal,
            )
        ),
      ];
    } else {
      return [
        ColumnsItemDuoble(
          label1: document.doctype.substring(0, 1).toUpperCase() + document.doctype.substring(1).toLowerCase() + '.',
          value1: document.docnum,
          label2: 'Fecha.',
          value2: document.datetrx,
        ),
        ColumnsItemDuoble(
          label1: 'Total',
          value1: document.amount,
          label2: 'Saldo',
          value2: document.balance,
        ),
      ];

    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: _getData()
            ),
          ),
        ),
        splashColor: Colors.black45,
        onTap: () async {
        },
      ),
    );
  }
}