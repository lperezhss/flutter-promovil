import 'dart:io';

import 'package:flutter/material.dart';

import '../route/route.dart' as route;

class Exit extends StatelessWidget {
  const Exit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - Salir')),
        body: GridView.count(
            crossAxisCount: 2,
            crossAxisSpacing: 4.0,
            mainAxisSpacing: 8.0,
            children: List.generate(choices.length, (index) {
              return Center(
                child: SelectCard(choice: choices[index]),
              );
            })
        )
    );
  }
}

class Choice {
  const Choice({required this.title, required this.icon, required this.routeName});
  final String title;
  final IconData icon;
  final String routeName;
}

const List<Choice> choices = <Choice>[
  Choice(title: 'Si', icon: Icons.check, routeName: route.emptyPage),
  Choice(title: 'No', icon: Icons.close, routeName: route.emptyPage),
];

class SelectCard extends StatelessWidget {
  const SelectCard({Key? key, required this.choice}) : super(key: key);
  final Choice choice;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: const EdgeInsets.all(15),
      elevation: 10,
      color: Colors.white,
      child: InkWell(
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Icon(
                      choice.icon,
                      size:80.0,
                      color: Colors.blueGrey,
                    )
                ),
                Text(choice.title,
                    style: const TextStyle(
                      fontSize: 25,
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                    )
                ),
              ]
          ),
        ),
        splashColor: Colors.black45,
        onTap: () {
          if (choice.title == 'Si') {
            exit(0);
          } else {
            Navigator.pop(context);
          }
        },
      ),
    );
  }
}