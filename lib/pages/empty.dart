import 'package:flutter/material.dart';

class Empty extends StatelessWidget {
  const Empty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('ProMovil - En construcción')),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Icon(
                    Icons.construction,
                    size:80.0,
                    color: Colors.blueGrey,
                  ),
                  Text('Proximamente',
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.blueGrey,
                        fontWeight: FontWeight.bold,
                      )
                  ),
                ]
            ),
          ),
        ),
    );
  }
}