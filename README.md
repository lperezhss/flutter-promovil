# promovil

A new Flutter project.

## Generate Part

```
flutter pub upgrade --major-versions
flutter packages upgrade
flutter pub outdated
flutter pub run build_runner build
```

## Description

App developed in Flutter for taking orders from foreign sellers.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
